from easy_net.server import LineProtocol, LineFactory, protocol
from easy_net.models import Message
from game import Game
from world import Snake
import attr


@attr.s
class User:
    name = attr.ib(type=str)
    snake = attr.ib(type=Snake, default=None)


server = LineFactory()
connections = []


@server.handle('login')
async def login(message: Message):
    protocol.user = User(message.data['name'])


@server.handle('join')
async def join(_):
    snake = game.field.add_new_snake(protocol.user)
    protocol.user.snake = snake
    connections.append(protocol.copy())


@server.handle('move')
async def move(message: Message):
    if message.data['direction'] == 'left':
        protocol.user.snake.turn_left()
    if message.data['direction'] == 'right':
        protocol.user.snake.turn_right()


@server.on_close()
async def on_close(*_):
    if protocol.user and protocol.user.snake:
        connections.remove(protocol.user)
        protocol.user.snake.field.kill_snake(protocol.user.snake)


if __name__ == '__main__':
    game = Game(connections)
    game.start()
    server.run(8956)
