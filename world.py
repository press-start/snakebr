import pygame
from random import randint
import json
from config import *


class Field:
    width = WIDTH
    height = HEIGHT
    food_spawn_period = 100

    def __init__(self):
        self.snakes = {}  # dict of snakes. User name -> Snake
        self.everything = [[Block(line * 8, cell * 8) for cell in range(self.width // 8)] for line in
                           range(self.height // 8)]  # 2D array of blocks
        self._current_step = 0

    def update(self):
        for snake in self.snakes.values():
            snake.update()
            self.check_collision(snake)
            self.spawn_food()

    def add_new_snake(self, user):
        current = Snake(self, 'test', 1)
        self.snakes[user.name] = current
        current.spawn_tail(current.def_length)
        return current

    @staticmethod
    def kill_snake(snake):
        for good in snake.parts:
            good.type = 'food'

    def spawn_food(self):
        self._current_step = (self._current_step + 1) % self.food_spawn_period
        if self._current_step:
            return
        x = randint(0, self.width // 8 - 1)
        y = randint(0, self.height // 8 - 1)

        self.everything[y][x].type = 'food'

    def check_collision(self, snake):
        if snake.head.x <= 0 or snake.head.x >= self.width or snake.head.y <= 0 or snake.head.y >= self.height:
            self.kill_snake(snake)

        current = self.everything[snake.head.get_y()][snake.head.get_y()]
        if current.type == 'empty':
            return
        elif current.type == 'food':
            snake.spawn_tail()
            current.type = 'empty'
        elif current.type == 'snake':
            self.kill_snake(snake)


class Block(pygame.Rect):
    size = 8

    def __init__(self, x, y, block_type='empty'):
        self.type = block_type
        super().__init__((self.size, self.size), (x * 8, y * 8))

    def set_coord(self, x, y):
        self.x, self.y = x * 8, y * 8

    def get_x(self):
        return self.x // 8

    def get_y(self):
        return self.y // 8

    def to_json(self):
        return [self.type, self.x, self.y]


class Snake:
    def_length = 10
    speed = 2

    def __init__(self, field, name, direction):
        self.field = field
        self.name = name  # STR name of each Snake
        self.direction = direction  # Direction in 0123 = NESW
        self.parts = []
        self._current_step = 0

    @property
    def head(self):
        return self.parts[0]

    def turn_right(self):
        self.direction = (self.direction + 1) % 4

    def turn_left(self):
        self.direction = (self.direction - 1) % 4

    def update(self):
        self._current_step = (self._current_step + 1) % Block.size
        if self._current_step:
            return

        tail = self.parts.pop()
        if self.direction == 0:
            tail.x, tail.y = self.head.x, self.head.y - 1
            self.parts.insert(0, tail)
        elif self.direction == 1:
            tail.x, tail.y = self.head.x + 1, self.head.y
            self.parts.insert(0, tail)
        elif self.direction == 2:
            tail.x, tail.y = self.head.x, self.head.y + 1
            self.parts.insert(0, tail)
        else:
            tail.x, tail.y = self.head.x - 1, self.head.y
            self.parts.insert(0, tail)

    def spawn_tail(self, def_length=1):

        if self.parts:
            x, y = self.head.x, self.head.y
        else:
            x, y = randint(0, 10), randint(0, 10)

        for i in range(1, def_length):
            if self.direction == 0:
                part = Block(x, (y + 1) * i, block_type='snake')
            elif self.direction == 1:
                part = Block((x - 1) * i, y, block_type='snake')
            elif self.direction == 2:
                part = Block(x, (y - 1) * i, block_type='snake')
            else:
                part = Block((x + 1) * i, y, block_type='snake')
            part.type = 'snake'
            self.parts.append(part)
            self.field.everything[part.get_y()][part.get_x()] = part
