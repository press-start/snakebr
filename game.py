from twisted.internet import task
from world import Field
from config import TPS
from easy_net.models import Message


class Game:
    def __init__(self, connections):
        self.connections = connections
        self.field = Field()
        self.task = task.LoopingCall(self.update)

    def start(self):
        self.task.start(1 / TPS)

    def update(self):
        self.field.update()
        self.shout()

    def shout(self):
        for protocol in self.connections:
            protocol.send(Message(
                'update', {
                    'snakes': [
                        [block.to_json() for block in snake.parts] for snake in self.field.snakes.values()
                    ]
                }
            ))
