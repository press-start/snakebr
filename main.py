from pygame.locals import *
import pygame
from config import *
from client import Client

BG_COLOR = (107, 142, 35)


class Snake:
    class Block(pygame.Rect):

        m_size = (8, 8)

        def __init__(self, x, y):
            super().__init__((x, y), self.m_size)

    def __init__(self, screen, data_blocks):
        self.screen = screen
        self.step = self.Block.m_size[0] + 2

        self.blocks = [self.Block(block[1], block[2]) for block in data_blocks]
        self.vector = (1, 0)

    def update(self):
        for i in range(len(self.blocks) - 1):
            segment = self.blocks[i]
            x, y = self.blocks[i + 1].x, self.blocks[i + 1].y
            segment.x, segment.y = x, y

        x, y = self.blocks[-2].x, self.blocks[-2].y

        self.blocks[-1].x = x + self.vector[0] * self.step
        self.blocks[-1].y = y + self.vector[1] * self.step

    def move_up(self):
        self.vector = (0, -1)

    def move_down(self):
        self.vector = (0, 1)

    def move_left(self):
        self.vector = (-1, 0)

    def move_right(self):
        self.vector = (1, 0)

    def render(self):
        for i in self.blocks:
            pygame.draw.rect(self.screen, (0, 0, 0), i)


class Block(pygame.Rect):
    size = 8

    def __init__(self, x, y, block_type='empty'):
        self.type = block_type
        super().__init__((x, y), (self.size, self.size))

    def set_coord(self, x, y):
        self.x, self.y = x * 8, y * 8

    def get_x(self):
        return self.x // 8

    def get_y(self):
        return self.y // 8

    def render(self, screen):
        pygame.draw.rect(screen, (0, 0, 0), self)


class World:
    def __init__(self, _game):
        self.game = _game

        self.everything = [[Block(line * 8, cell * 8) for cell in range(self.game.width // 8)] for line in
                           range(self.game.height // 8)]  # 2D array of blocks

        self.snakes = []

    #
    # def update(self):
    #     """
    #     Update all objects inside world
    #     """
    #     self.snakes[0].update()

    def render(self):
        """
        Draw all objects inside world.
        """
        for snake in self.snakes:
            snake.render()

        # for block_list in self.everything:
        #     for block in block_list:
        #         if block.type != 'empty':
        #             block.render(self.game.screen)


class Game:
    FPS = 10

    def __init__(self, display_size: tuple):
        pygame.init()
        self.display = display_size
        self.width = display_size[0]
        self.height = display_size[1]
        self.screen = pygame.display.set_mode(display_size)

        pygame.display.set_caption("SnakeBR")
        self.bg = pygame.Surface(self.display)
        self.bg.fill(BG_COLOR)

        self.clock = pygame.time.Clock()
        self.world = World(self)

    def run(self):
        while True:
            self.screen.blit(self.bg, (0, 0))
            self.process_input()
            self.world.render()

            pygame.display.update()
            self.clock.tick(self.FPS)

    #
    def process_input(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                exit()
            if event.type == KEYDOWN:

                if event.key == K_d or event.key == K_RIGHT:
                    client.send('move', {'direction': 'right'})

                if event.key == K_a or event.key == K_LEFT:
                    client.send('move', {'direction': 'left'})

                if event.key == K_o:
                    self.FPS += 1
                    print("FPS Increased to {}".format(self.FPS))

                if event.key == K_l:
                    self.FPS -= 1
                    print("FPS Decreased to {}".format(self.FPS))


class Food():
    def __init__(self, food_color):
        """Инит еды"""
        self.food_color = food_color
        self.food_size_x = 10
        self.food_size_y = 10
        self.food_pos = [0, 0]

    def draw_food(self, play_surface):
        """Отображение еды"""
        pygame.draw.rect(
            play_surface, self.food_color, pygame.Rect(
                self.food_pos[0], self.food_pos[1],
                self.food_size_x, self.food_size_y))


client = Client()


@client.handle('update')
def on_update(data):
    game.world.snakes = []
    for snake in data['snakes']:
        game.world.snakes.append(Snake(game.screen, snake))


if __name__ == '__main__':
    game = Game((WIDTH, HEIGHT))
    client.send('login', {'name': input()})
    client.send('join', {})
    game.run()
